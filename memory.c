#include "globals.h"

/* Expression size limit */
#define NUM_TREES 2048

static tree_t trees=NULL;
static int treedex;

/* 
 * Return a pointer to a tree struct that is guaranteed not to be used by 
 * anyone else until reset_trees() is called. 
 */
tree_t alloc_tree() {
    if (treedex >= NUM_TREES) {
        /* When every element in buffer has been used, complain */
        fprintf(stderr, "Slab allocator for trees is out of memory.\n");
        exit(1);
    }
    
    return &trees[treedex++];
}

/*
 * Mark all previously allocated tree structs as available for reuse
 */
void reset_trees() {
    if (trees == NULL) {
        /* Allocate memory on first use */
        trees = malloc(sizeof(struct tree_struct) * NUM_TREES);
    }
    treedex = 0;
}

/* maximum number of rational_t structs to be allocated */
#define NUM_NUMBERS (1 << 10)


/* This slab is structured as a ring buffer because, after parsing one
 * expression, an extra token (extra number) is loaded into memory and
 * wiping from the start would overwrite that token. */
/* This program is totally not a house of cards. */

static rational_t* numbers = NULL;
static int ratdex;
static int start;

rational_t* alloc_number() {
    if (ratdex == start) {
        /* When every element in buffer has been used, complain */
        fprintf(stderr, "Slab allocator for numbers is out of memory.\n");
        exit(1);
    }

    rational_t* t = &numbers[ratdex];
    /* move to next element in circular buffer */
    ratdex++;
    if (ratdex == NUM_NUMBERS) {
        ratdex = 0;
    }
    return t;
}

/*
 * Mark all previously allocated rational_t structs as available for reuse,
 * except for the last one to be allocated.
 */
void reset_numbers() {
    if (numbers == NULL) {
        /* Allocate memory on first use */
        numbers = malloc(sizeof(rational_t) * NUM_NUMBERS);
        ratdex = 0;
        start = 0;
    }
    /* Mark new end point */
    start = ratdex - 1;
}
