#include "globals.h"

/* TASK
 * 4. Evaluate the parse tree and output the result to stdout.
 */

/*
 * Compute the greatest-common-factor of the input values  
 * and divide both by that factor.
 */
static inline bigint_t reduce(bigint_t* a, bigint_t* b) {
    /* normalize to positive */
    bigint_t a2 = *a < 0 ? -*a : *a;
    bigint_t b2 = *b < 0 ? -*b : *b;
    
    /* special case when one is 0 */
    if (b2 == 0) {
        return 1;
    }
    /* establish b2 > a2 and do useful work */
    if (a2 > b2) {
        a2 %= b2;
    }

    /* at while test, b2 > a2 */
    while (a2 != 0) {
        b2 %= a2;
        /* here, a2 > b2 */
        if (b2 == 0) {
            break;
        }
        a2 %= b2;
    }

    /* pick the larger value of a2/b2 which is the gcf */
    bigint_t gcf = a2 == 0 ? b2 : a2;
    *a /= gcf;
    *b /= gcf;
    return gcf;
}

/*
 * Modulo function that maps all number pairs (a,b) onto [0,|b|) 
 */
static inline bigint_t magic_mod(bigint_t a, bigint_t b) {
    /* We let n % 0 be 0 due to squeeze arguments */
    if (b == 0) {
        return 0;
    }
    /* fix sign of b to be positive */
    if (b < 0) {
        b = -b;
    }
    
    if (a < 0) {
        /* handle negative case; C-modulo of negative numbers are negative */
        bigint_t c = (a % b);
        if (c == 0) {
            return 0;
        } else {
            return b + c;
        }
    } else {
        /* positive case is simple */
        return a % b;
    }
}

/*
 * Evaluate a recursive tree of tree_t elements. Passes fat rational_t by value.
 */
rational_t eval_tree(tree_t t) {
    /* r will be the output value; s and q intermediate results */
    rational_t r;
    r.num = 0;
    r.denom = 1;
    rational_t s;
    rational_t q;
    bigint_t gcf = 0;
    /* switch on the type of operation for the tree
     *
     * We don't reduce fractions after addition/subtracting/modding
     * since this rarely makes a difference (and the other operators 
     * reduce the result themselves)
     */
    switch (t->op) {
        case O_NUM:
            r = *t->value;
            reduce(&r.num, &r.denom);
            break;
        case O_ADD:
            /* addition of a/kb + c/kd = (ab+cd)/kbd */
            s = eval_tree(t->first);
            q = eval_tree(t->second);
            gcf = reduce(&s.denom, &q.denom);
            r.denom = gcf * q.denom * s.denom;
            r.num = (s.num * q.denom) + (q.num * s.denom);
            break;
        case O_SUB:
            /* subtraction of a/kb - c/kd = (ab-cd)/kbd */
            s = eval_tree(t->first);
            q = eval_tree(t->second);
            gcf = reduce(&s.denom, &q.denom);
            r.denom = gcf * q.denom * s.denom;
            r.num = (s.num * q.denom) - (q.num * s.denom);
            break;
        case O_MOD:
            /* modulo of a/kb % c/kd = (ab%cd)/kbd */
            s = eval_tree(t->first);
            q = eval_tree(t->second);
            gcf = reduce(&s.denom, &q.denom);
            r.denom = gcf*q.denom*s.denom;
            r.num = magic_mod(s.num*q.denom,q.num*s.denom);
            break;
        case O_DIV:
            /* dividing a/b by c/d gives ad/bc */
            s = eval_tree(t->first);
            q = eval_tree(t->second);
            r.denom = s.denom * q.num;
            r.num = s.num * q.denom;
            if (r.denom < 0) {
                /* normalize so that  */
                r.num = -r.num;
                r.denom = -r.denom;
            }
            /* simplify result */
            reduce(&r.num, &r.denom);
            break;
        case O_MUL:
            /* multiplying a/b by c/d gives ac/bd */
            s = eval_tree(t->first);
            q = eval_tree(t->second);
            r.denom = s.denom * q.denom;
            r.num = s.num * q.num;
            reduce(&r.num, &r.denom);
            break;
        case O_LINC:
            /* left increment: a/b+1=(a+b)/b */
            s = eval_tree(t->first);
            r.denom = s.denom;
            r.num = s.num + s.denom;
            break;
        case O_LDEC:
            /* left decrement: a/b-1=(a-b)/b */
            s = eval_tree(t->first);
            r.denom = s.denom;
            r.num = s.num - s.denom;
            break;
        case O_NEG:
            /* negation applied to numerator, not denominator*/
            s = eval_tree(t->first);
            r.denom = s.denom;
            r.num = -s.num;
            break;
        case O_RINC:
        case O_RDEC:
        case O_NOOP:
            /* postfix operators do nothing; instead, parenthesis technically
             * are their point of evaluation */
            r = eval_tree(t->first);
            break;
        case O_PAREN:
            /* either pass through the child value or apply postfix operators
             * if they are a direct child. */
            switch (t->first->op) {
                case O_RINC:
                    s = eval_tree(t->first->first);
                    r.denom = s.denom;
                    r.num = s.num + s.denom;
                    break;
                case O_RDEC:
                    s = eval_tree(t->first->first);
                    r.denom = s.denom;
                    r.num = s.num - s.denom;
                    break;
                default:
                    r = eval_tree(t->first);
                    break;
            }
            break;
        default:
            /* Error clause, should never happen */
            fprintf(stderr, "Unidentified %d\n", t->op);
            exit(1);
            break;
    }

    /* return the calculated value for this tree node */
    return r;
}
