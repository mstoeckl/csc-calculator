#include <stdlib.h>
#include <stdio.h>
#include "globals.h"

/* 
 * Slightly optimized routine to output a rational number.
 * Gives output with 6 digits after the decimal and rounds even.
 */
static void write_result(rational_t r, FILE* output) {
    /* buffer is overkill */
    char buf[40];
    /* sign handling; take abs for common code path */
    int k=0;
    if (r.num < 0) {
        k = 1;
        r.num = -r.num;
    }

    /* happens on imperfectly constructed test suites. */
    if (r.denom == 0) {
        fwrite("NaN\n", sizeof(char), 5, output);
        return;
    }

    char* p = buf+39;
    char* end = p;
    *p-- = '\0';
    *p-- = '\n';

    int64_t result = (r.num * 1000000) / r.denom;
    int64_t oops = 2 * r.num * 1000000 - r.denom * (2 * result + 1);
    /* We round up if the floor division for result is off by more than
     * half a last digit. Also round up for odd result values and down
     * for even result values if the next digit would be _exactly_ 5.*/
    if (oops > 0 || (oops == 0 && result % 2 == 1)) {
        result++;
    }

    if (result >= 1000000) {
        /* case when decimal point happens _inside_ the number
        * ex. 1.000000, 1.243422, 5234234.025254 */
        while (end - p < 8) {
                *p-- = (char)(result % 10 + 0x30);
            result = result / 10;
        }
        *p-- = '.';
        while (result > 0) {
            *p-- = (char)(result % 10 + 0x30);
            result = result / 10;
        }
    } else {
        /* case when decimal point happens _before_ the number proper
         * ex. 0.000111, 0.243422, 0.022222 */
        while (result > 0) {
            *p-- = (char)(result % 10 + 0x30);
            result = result / 10;
        }
        while (end - p < 8) {
            *p-- = '0';
        }
        *p-- = '.';
        *p-- = '0';
    }
    /* handle odd numbers */
    if (k) {
        *p-- = '-';
    }
    /* print the result to the output stream */
    p++;
    fwrite(p, sizeof(char), end - p, output);
}

/**
 * Debugging function. Gives string representation for a token.
 * As soon as this is called again the old string may be corrupted.
 */
static char* str_token(tok_t x) {
    /* oversized buffer to hold number output */
    static char buf[50];
    if (x.tk == T_NUM) {
        /* Using floating point printing for values.
         * May differ from true result. */
        sprintf(buf, "NUM %f %p\t", x.value->num/(double)x.value->denom,
                x.value);
        return buf;
    } else {
        switch (x.tk) {
            /* EOF and EOS end statements */
            case T_EOF: return "EOF\n";
            case T_EOS: return "EOS\n";
            /* All other intermediate tokens followed by tab */
            case T_LPAREN: return "LPAREN\t";
            case T_RPAREN: return "RPAREN\t";
            case T_INC: return "INC\t";
            case T_DEC: return "DEC\t";
            case T_ADD: return "ADD\t";
            case T_SUB: return "SUB\t";
            case T_MUL: return "MUL\t";
            case T_MOD: return "MOD\t";
            case T_DIV: return "DIV\t";
            default: return "???\t";
        }
    }
}

/**
 * Debugging function. Gives string representation for a tree element
 */
static char* str_tree(operator_class o, rational_t* v, char* buf) {
    if (o == O_NUM) {
        /* Using floating point printing for values.
         * May differ from true result. */
        sprintf(buf, "NUM %f %p\t", v->num/(double)v->denom, v);
        return buf;
    } else {
        switch (o) {
            case O_PAREN: return "PAREN\t";
            case O_LINC: return "LINC\t";
            case O_RINC: return "RINC\t";
            case O_LDEC: return "LDEC\t";
            case O_RDEC: return "RDEC\t";
            case O_NOOP: return "NOOP\t";
            case O_NEG: return "NEG\t";
            case O_ADD: return "ADD\t";
            case O_SUB: return "SUB\t";
            case O_MUL: return "MUL\t";
            case O_MOD: return "MOD\t";
            case O_DIV: return "DIV\t";
            default: return "???\t";
        }
    }
}

/**
 * Print tree structure recursively. To use, call with ident=0.
 */
static void emit_tree(tree_t t, int ident) {
    if (!t) {
        return;
    }
    /* create custom format string to handle arbitrary indentation */
    const char* fmt = "%%%dc%%d:%%p|%%s\n";
    char i2[20];
    sprintf(i2, fmt, ident+1);
    char buf[40];
    char* b = str_tree(t->op, t->value, buf);
    fprintf(stderr, i2, ' ', t->op, t, b);
    emit_tree(t->first, ident+1);
    emit_tree(t->second, ident+1);
}

/**
 * Program entry point.
 */
int main (int argc, char *argv[]) {
    FILE *fp;
    FILE* fout;
    /* Argument handling */
    if (argc < 2) {
        fprintf(stderr, "Usage: ./calculator infile.txt [outfile.txt]\n");
        exit(1);
    }
    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        fprintf(stderr, "Error: infile.txt does not exist.\n");
        exit(1);
    }
    if (argc == 3) {
        fout = fopen(argv[2],"w");
        if (fout == NULL) {
            fprintf(stderr, "Error: outfile.txt can not be written.\n");
            exit(1);
        }
    } else {
        /* Default to stdout if no file is given. */
        fout = stdout;
    }

    /* Configure reader, */
    initialize_reader(fp);
    /* Set up memory slabs. */
    reset_trees();
    reset_numbers();

    /* 
     * The following debugging options dump intermediate
     * values.
     */
    if (getenv("DWIM")) {
        /* Give tokenized form of input. */
        while (1) {
            /* read one token */
            tok_t x = next_token();
            /* print token description */
            fprintf(stderr, "%s", str_token(x));
            if (x.tk == T_EOF) {
                /* EOF only occurs once so we stop. */
                break;
            }
            if (x.tk == T_EOS) {
                /* wipe memory buffer, reset */
                reset_numbers();
            }
        }
    } else if (getenv("DWIM2")) {
        /* Give tree form of input. */
        /* Preload first token for parser */
        ready_parser();
        while (1) {
            /* parse next statement and create a tree */
            tree_t tree = next_stmt();
            if (!tree) {
                /* null tree signals EOF */
                break;
            }
            /* print out tree */
            emit_tree(tree,0);
            /* wipe memory buffers to prepare for next run */
            reset_trees();
            reset_numbers();
        }
    } else if (getenv("DWIM3")) {
        /* Give preprocessed input. */
        while (1) {
            /* read current character */
            char c = get_char();
            if (c == EOF) {
                /* if EOF appears in stream we are done */
                break;
            }
            /* write character to stdout */
            putc(c, stdout);
            /* pull in next character */
            advance_char();
        }
    } else {
        /* Parse and evaluate expressions */
        /* Preload first token for parser */
        ready_parser();
        while (1) {
            /* parse next statement and create a tree */
            tree_t tree = next_stmt();
            if (!tree) {
                /* null tree signals EOF */
                break;
            }
            /* evaluate and print results */
            rational_t val = eval_tree(tree);
            write_result(val, fout);
            /* Wipe and reset memory buffers */
            reset_trees();
            reset_numbers();
        }
    }

    /* cleanup reading apparatus */
    finalize_reader();
    /* 4) success! */
    exit(0);
}
 
