#!/usr/bin/python3

from random import *

pressure = 0
if True:
    idco = ["(--)","(--)","(++)","(++)","(+)","(-)"]
else:
    idco = ["--"," --","++"," ++", " +", " -"]

def skewed_random():
    global pressure
    x = random()
    x = 1 - x**(1 + pressure / 20)
    pressure += 1
    return x

def make_number():
    x = 1.001 / (0.001 + random()) - random()
    if skewed_random() > 0.5:
        x = round(x)
    else:
        x = round(x,6)
    return [x], str(x)

def make_D(ev,er):
    y = skewed_random()
    if y < 0.05:
        return make_D(["--", ev], er + idco[0])
    elif y < 0.10:
        return make_D(["++", ev], er + idco[2])
    else:
        return ev,er

def make_C():
    y = skewed_random()
    if y < 0.5:
        ev,er = make_E()
        return make_D(["(",ev], "(" + er + ")")
    else:
        ev,er = make_number()
        return make_D(ev,er)

def make_F():
    y = skewed_random()
    if y < 0.05:
        fv,fr = make_F()
        return ([" -",fv], idco[5] + fr)
    elif y < 0.10:
        fv,fr = make_F()
        return ([" +",fv], idco[4] + fr)
    elif y < 0.15:
        fv,fr = make_F()
        return ([" --",fv], idco[1] + fr)
    elif y < 0.20:
        fv,fr = make_F()
        return ([" ++",fv], idco[3] + fr)
    else:
        return make_C()

## Cutoff 2
#make_F = make_number

def make_B(fv,fr):
    y = skewed_random()
    if y < 0.2:
        fv2,fr2 = make_F()
        return make_B(["*",fv,fv2], fr + " * " + fr2)
    elif y < 0.3:
        fv2,fr2 = make_F()
        if eval_tree(fv2) == 0:
            return fv,fr
        return make_B(["/",fv,fv2], fr + " / " + fr2)
    elif y < 0.4:
        fv2,fr2 = make_F()
        if eval_tree(fv2) == 0:
            return fv,fr
        return make_B(["%",fv,fv2], fr + " % " + fr2)
    else:
        return fv,fr

def make_T():
    fv,fr = make_F()
    return make_B(fv, fr)

## Cutoff 1
#make_T = make_number

def make_A(tv,tr):
    y = skewed_random()
    if y < 0.2:
        tv2,tr2 = make_T()
        return make_A(["+",tv,tv2], tr + " + " + tr2)
    elif y < 0.4:
        tv2,tr2 = make_T()
        return make_A(["-",tv,tv2], tr + " - " + tr2)
    else:
        return tv,tr

def make_E():
    tv,tr = make_T()
    return make_A(tv, tr)

def make_S():
    global pressure
    pressure = 0
    v,s = make_E()
    return v, s + ";"

def eval_tree(t):
    if t[0] == "+":
        return eval_tree(t[1]) + eval_tree(t[2])
    elif t[0] == "-":
        # subtract
        return eval_tree(t[1]) - eval_tree(t[2])
    elif t[0] == "--":
        # post-decrement (do nothing, handled by parenthesis operator)
        return eval_tree(t[1])
    elif t[0] == " --":
        # pre-decrement
        return eval_tree(t[1]) - 1
    elif t[0] == "++":
        # post-increment (do nothing, handled by parenthesis operator)
        return eval_tree(t[1])
    elif t[0] == " ++":
        # pre-increment
        return eval_tree(t[1]) + 1
    elif t[0] == " +":
        # pre-no-op
        return eval_tree(t[1])
    elif t[0] == " -":
        # pre-negate
        return -eval_tree(t[1])
    elif t[0] == "/":
        return eval_tree(t[1]) / eval_tree(t[2])
    elif t[0] == "%":
        return eval_tree(t[1]) % abs(eval_tree(t[2]))
    elif t[0] == "*":
        return eval_tree(t[1]) * eval_tree(t[2])
    elif t[0] == "(":
        # post inc/dec special case
        if t[1][0] == "--":
            return eval_tree(t[1][1]) - 1
        elif t[1][0] == "++":
            return eval_tree(t[1][1]) + 1
        else:
            return eval_tree(t[1])
    else:
        return t[0]

if __name__ == "__main__":
    import sys
    num = 10
    loud = True
    if len(sys.argv) == 2:
        num = int(sys.argv[1])
        loud = False

    with open("in.txt","w") as inpf, open("out.txt","w") as outpf:
        for _ in range(num):
            t,s = make_S()
            v = eval_tree(t)
            if v == -0.0:
                v = abs(v)
            inpf.write(s+"\n")
            outpf.write("{:.6f}\n".format(round(v,6)))
            if loud:
                print("{} --> {:.6f};".format(s, round(v,6)))