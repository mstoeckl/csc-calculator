#include "globals.h"

/* TASK
 * 2. Scan the buffer into tokens.
 */

/* 
 * Short routine to parse a number. Starts at the first character in the number
 * and advances to the character after the number.
 */
static rational_t* parse_number() {
    /* it's cleaner to pull this out of the DFA proper */
    /* request a number from the slab allocator */
    rational_t* rt = alloc_number();
    rt->num = 0;
    rt->denom = 1;
    /* every digit read incrementally increases the value of the number */
    uint8_t d;
    while ((d = get_char() - 0x30) < 10) {
        rt->num = rt->num*10 + d;
        advance_char();
    }

    /* if the non-digit in the current position is not a dot (.), we are done */
    if (d != 0xFE) {
        return rt;
    }
    /* move to the (hopefully) digit after the decimal point */
    advance_char();
    /* read digits incrementally and add them into the
     * right location in the number */
    while ((d = get_char() - 0x30) < 10) {
        rt->num = rt->num*10 + d;
        rt->denom = rt->denom * 10;
        advance_char();
    }
    /* return the pointer to the rational number */
    return rt;
}

/*
 * Return the next token in the input sequence; one with .tk=T_EOF is given
 * when the character input stream ends.
 */
tok_t next_token() {
    /* four states, since only two simple tokens have more than one character */
    enum {
        S_START,
        S_GOT_PLUS,
        S_GOT_MINUS,
        S_DONE
    } state = S_START;
    /* token to return */
    tok_t tok;
    
    /* helper macro to set the token type, move to the next char,
       and set the state so we return the token */
#define ACCEPT(token) {advance_char();tok.tk = token;state = S_DONE;}

    /* continue until a token has been recognized */
    while (state != S_DONE) {
        /* get the current character */
        char c = get_char();
        switch (state) {
            case S_START:
                /* handle a new token; so switch on its first character */
                switch (c) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case '.':
                        /* go into digit parsing routine */
                        tok.value = parse_number();
                        tok.tk = T_NUM;
                        state = S_DONE;
                        break;
                    case '+':
                        /* go to the next character; token could be + or ++ */
                        advance_char();
                        state = S_GOT_PLUS;
                        break;
                    case '-':
                        /* go to the next character; token could be - or -- */
                        advance_char();
                        state = S_GOT_MINUS;
                        break;
                    /* all the following tokens are single character*/
                    case '%':
                        ACCEPT(T_MOD);
                        break;
                    case '/':
                        ACCEPT(T_DIV);
                        break;
                    case '*':
                        ACCEPT(T_MUL);
                        break;
                    case ';':
                        ACCEPT(T_EOS);
                        break;
                    case '(':
                        ACCEPT(T_LPAREN);
                        break;
                    case ')':
                        ACCEPT(T_RPAREN);
                        break;
                    case EOF:
                        ACCEPT(T_EOF);
                        break;
                    default:
                        /* no input so we abort */
                        advance_char();
                        break;
                }
                break;
            case S_GOT_PLUS:
                /* next char determines if it's '+' or '++' */
                switch (c) {
                    case '+':
                        /* it's increment */
                        ACCEPT(T_INC);
                        break;
                    default:
                        /* next character isn't + so it's just the ADD
                         * token. We have already advanced a character that
                         * isn't part of the token so we don't need to again*/
                        tok.tk = T_ADD;
                        state = S_DONE;
                        break;
                }
                break;
            case S_GOT_MINUS:
                /* next char determines if it's '-' or '--' */
                switch (c) {
                    case '-':
                        /* it's decrement */
                        ACCEPT(T_DEC);
                        break;
                    default:
                        /* next character isn't - so it's just the SUB
                         * token. We have already advanced a character that
                         * isn't part of the token so we don't need to again*/
                        tok.tk = T_SUB;
                        state = S_DONE;
                        break;
                }
                break;
            case S_DONE:
                /* case never occurs but there for completeness. */
                break;
        }
    }

#undef ACCEPT

    /* return the token just extracted */
    return tok;
}
