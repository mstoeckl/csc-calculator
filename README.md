# WHY

For the 2015 University of Rochester course, Computation and Formal Systems.

# HOW TO COMPILE

Run

    $ make

or

    $ make debug

depending on your ulterior motives.

# HOW TO USE

To run the program, walk up to the teleprinter and type in the prompt

    $ ./calculator infile.txt outfile.txt

And the program should process everything in `infile.txt` and write its output
to `outfile.txt`. An example input file is:

    (+)7.367194 / (-)1.520052 / 0.651268 + (13 + 1.635391 % (--)4);
    (++)(1.197472 % 2.111586 - 1.089873 * 1.450914) - 18.000959;

and the corresponding output of the program is:

    7.193489
    -17.384799

Mathematical operations supported are addition "+", multiplication "*",
subtraction "-", negation "-", division "/", and modulo "%". Putting an
extra "+" in front of numbers does not change their value. Prefix ++ and --
(increment and decrement) operators increase the value they act on by 1. 
Postfix increment and decrement do not work as expected for immutable numbers;
instead, they only apply if directly enclosed by parenthesis.

# INPUT TOKENS

    num := 0|[1-9][0-9]*(.[0-9]+)?
    lpar := "("
    rpar := ")"
    inc := "++"
    dec := "--"
    add := "+"
    sub := "-"
    div := "/"
    mul := "*"
    mod := "%"
    eos := ";"
    eof := end of input

# INPUT GRAMMAR

    start   :=  E eos
        E   :=  T A
        A   :=  add T A
            |  sub T A
            |  ϵ
        T   :=  F B
        B   :=  mul F T
            |  div F T
            |  mod F T
            |  ϵ
        F   :=  inc F
            |  dec F
            |  add F
            |  sub F
            |  C
        C   :=  (E) D
            |  i D
        D   :=  inc D
            |  dec D
            |  ϵ

# BUGS

The program gives up on expressions that create a parse tree larger than about
2000 internal and leaf nodes. It gives up when more than about 1000 numbers are
present in a single expression. It's preferred mode of error handling in the
parser is to fail loudly (with a segfault) when it encounters an unexpected
token). Too complex expressions that overflow inefficiently calculated 128 bit
rational arithmetic will result in undefined behavior.

# SOURCE FILES AND DESCRIPTIONS

* evaluator.c - evaluate the expression trees created by the parser
* globals.h - header file to synchronize the source files
* main.c - coordination and command line argument handling. Number printing.
* memory.c - utilities to avoid the malloc/free penalty
* parser.c - LL(1) parser for a given token stream that creates expression trees
* reader.c - read out a text stream from a file
* scanner.c - lexer code (a DFA) to tokenize a text stream
* test.py - Test generation script

# MISC

Intermediate processing stages for the program can be dumped by setting one of 
the environment variables DWIM, DWIM2, and DWIM3 to any value.