#include "globals.h"

/* TASK
 * 3. Parse the tokens into a valid parse tree representing the expression.
 */

/* debugging helper macro */
#if 0
#define TRACK {fprintf(stderr, "%s %d %p\n",__FUNCTION__,token.tk,token.value);}
#else
#define TRACK
#endif

/* value of the current token to be matched */
static tok_t token;

/*
 * Match the current token; if the token is not what we expect, die. Otherwise,
 * return the token matched and advance to the next token.
 */
static tok_t match_or_die(token_class tok) {
    if (token.tk != tok) {
        /* die, creating a segfault for a free stack trace */
        fprintf(stderr, "Aborting. The segfault is for your convenience.\n");
        /* cleanup the reader while we're giving up. */
        finalize_reader();
        /* crash */
        (*(int*)(void*)0)++;
        /* not that we need to */
        exit(1);
    } else {
        /* match the token */
        tok_t temp = token;
        /* call for the next token in the series */
        token = next_token();
        /* return the token just matched */
        return temp;
    }
}

/*
 * Helper function to create a tree node for an operator and two children
 */
static tree_t make_node(operator_class op, tree_t first, tree_t second) {
    /* tree is allocated from slab system */
    tree_t tree = alloc_tree();
    tree->op = op;
    tree->first = (struct tree_struct*)first;
    tree->second = (struct tree_struct*)second;
    return tree;
}

/* The following parse_X functions handle the grammar and rewrite the tree
 * as necessary. This LL(1) parser is constructed from the following grammar.
 * 
 * S   :=  E eos
 * E   :=  T A
 * A   :=  add T A
 *         sub T A
 *         ϵ
 * T   :=  F B
 * B   :=  mul F T
 *         div F T
 *         mod F T
 *         ϵ
 * F   :=  inc F
 *         dec F
 *         add F
 *         sub F
 *         C
 * C   :=  (E) D
 *         i D
 * D   :=  inc D
 *         dec D
 *         ϵ
 * 
 */

static tree_t parse_D(tree_t a) {
    TRACK;
    /* handle the chain of post increment and decrements applied
       from left to right. */
    if (token.tk == T_INC) {
        /* post-increment */
        match_or_die(token.tk);
        /* continue parsing the same production */
        return parse_D(make_node(O_RINC,a,NULL));
    } else if (token.tk == T_DEC) {
        /* post-decrement */
        match_or_die(token.tk);
        /* continue parsing the same production */
        return parse_D(make_node(O_RDEC,a,NULL));
    } else {
        /* no more inc/dec tokens */
        return a;
    }
}

static tree_t parse_E();
static tree_t parse_C() {
    TRACK;
    if (token.tk == T_LPAREN) {
        /* handle parenthesized expression */
        match_or_die(T_LPAREN);
        tree_t a = parse_E();
        match_or_die(T_RPAREN);
        /* parse the following inc/dec tail */
        return parse_D(make_node(O_PAREN, a, NULL));
    } else {
        /* match a number and adopt its value */
        tok_t num = match_or_die(T_NUM);
        tree_t a = make_node(O_NUM, NULL, NULL);
        a->value = num.value;
        /* parse the following inc/dec tail */
        return parse_D(a);
    }
}

static tree_t parse_F() {
    TRACK;
    /* handle the all unary operators left of a number/paren. expr. */
    if (token.tk == T_INC || token.tk == T_DEC) {
        /* increment and decrement have the same operator values
         * as token values */
        tok_t op = match_or_die(token.tk);
        return make_node(op.tk,parse_F(),NULL);
    } else if (token.tk == T_ADD) {
        /* unary +: T_ADD != O_NOOP so we handle this specially */
        match_or_die(token.tk);
        return make_node(O_NOOP,parse_F(),NULL);
    } else if (token.tk == T_SUB) {
        /* unary -: T_SUB != O_NEG so we handle this specially */
        match_or_die(token.tk);
        return make_node(O_NEG,parse_F(),NULL);
    } else {
        /* no prefix operators, continue to number or thing in parens. */
        return parse_C();
    }
}

static tree_t parse_B(tree_t a) {
    TRACK;
    if (token.tk == T_MUL || token.tk == T_MOD || token.tk == T_DIV) {
        /* handle multiplication/division/modulo between factors */
        tok_t op = match_or_die(token.tk);
        /* load a factor */
        tree_t b = parse_F();
        /* pass tree to function to rewrite left-associatively */
        return parse_B(make_node(op.tk,a,b));
    } else {
        /* nothing else in this sequence connected by +, /, and % */
        return a;
    }
}

static tree_t parse_T() {
    TRACK;
    /* start parsing a sequence of factors connected by  +, /, and % */
    tree_t a = parse_F();
    return parse_B(a);
}

static tree_t parse_A(tree_t a) {
    TRACK;
    if (token.tk == T_ADD || token.tk == T_SUB) {
        /* handle addition/subtraction between terms */
        tok_t op = match_or_die(token.tk);
        tree_t b = parse_T();
        /* pass tree to function to rewrite left-associatively */
        return parse_A(make_node(op.tk,a,b));
    } else {
        /* nothing more in the +,- sequence of terms */
        return a;
    }
}

static tree_t parse_E() {
    TRACK;
    /* start parsing a sequence of terms connected with + and - tokens */
    tree_t a = parse_T();
    return parse_A(a);
}

static tree_t parse_S() {
    TRACK;
    /* parse the main nonterminal, an expression followed by 
     * an end-of-string token */
    tree_t a = parse_E();
    match_or_die(T_EOS);
    return a;
}

/*
 * Parse an expression from the token stream and return the expression tree.
 * Returns NULL if the end of input has been reached. 
 */
tree_t next_stmt() {
    /* The one case where we don't go up in flames on an unusual token */
    if (token.tk == T_EOF) {
        return NULL;
    }
    /* haven't reached end of input so parse! */
    return parse_S();
}
/*
 * Prepare the parser to be able to read expressions from input.
 */
void ready_parser() {
    /* need one token to start parsing */
    token = next_token();
}
