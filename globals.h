#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/* Tougher integers for tougher jobs */
typedef __int128_t bigint_t;


/* Representation for numbers in Q.
 * denom should be nonnegative
 * 0 is canonically represented as 0/1 */
typedef struct {
    bigint_t num;
    bigint_t denom;
} rational_t;

/* 12 token types (need >=4 bits of room) */
typedef enum {
    T_EOF=0,/* eof ($), also 0xFF */
    T_EOS=1,/* ; */
    T_NUM=2,/* decimal */
    T_LPAREN=4,/* ( */
    T_RPAREN=5,/* ( */
    T_INC=10,/* ++ */
    T_DEC=11,/* -- */
    T_ADD=12,/* + */
    T_SUB=13,/* - */
    T_MUL=14,/* * */
    T_MOD=15,/* % */
    T_DIV=16 /* / */
} token_class;

/* operator types value overlap with tokens because many tokens are
 * operators. This simplifies some parsing code.*/
typedef enum {
    O_NUM=T_NUM,
    O_PAREN=3,
    O_LINC=T_INC,
    O_RINC=6,
    O_LDEC=T_DEC,
    O_RDEC=7,
    O_ADD=T_ADD,
    O_SUB=T_SUB,
    O_NOOP=8,
    O_NEG=9,
    O_MOD=T_MOD,
    O_MUL=T_MUL,
    O_DIV=T_DIV    
} operator_class;

/* Token has a type (int) and optionally used pointer to a number */
typedef struct {
    token_class tk;
    rational_t* value;
} tok_t;

/* Tree element, with optional pointer to rational value, operator type
 * and pointer to up to two children.*/
typedef struct tree_struct {
  rational_t* value;
  struct tree_struct* first;
  struct tree_struct* second;
  operator_class op;
}* tree_t;

/* evaluator.c */
rational_t eval_tree(tree_t);

/* parser.c */
void ready_parser();
tree_t next_stmt();

/* scanner.c */
tok_t next_token();

/* reader.c */
void initialize_reader(FILE*);
void finalize_reader();
char get_char();
char advance_char();

/* memory.c */
tree_t alloc_tree();
void reset_trees();
rational_t* alloc_number();
void reset_numbers();