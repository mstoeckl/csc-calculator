#include "globals.h"

/* TASK
 * 1. Read the to-be-evaluated expression from an input file,
 * which contains a single (maybe multiline), valid, arithmetic
 * expression. We will NOT test error handling.
 */

/* Wherein we suddenly eat characters and spit them back out not unchanged. */
#define DOOFUS_MODE 1

/* Size of internal buffer */
#define BUFLEN (1 << 16)

static FILE* f;
static char buffer[BUFLEN+1];

/* Variables to control reading */
static int copylen;
static int i;
static int cap;
static int dead;

/**
 * Set up reading apparatus and ensure that the next calls succeed.
 */
void initialize_reader(FILE* ff) { 
    f = ff;
    cap = 0;
    dead = 0;
    /* temporarily set copylen to zero so that we fill the buffer
     * from the start */
    copylen = 0;
    advance_char();
    /* five characters lookahead are enough for anyone */
    copylen = 5;
}
void finalize_reader() {
    /* cleanup file, not that it matters */
    fclose(f);
}

char get_char() {
    /* access current character */
    return buffer[i];
}

char advance_char() {
    if (dead) {
        /* Once no new data is incoming stop incrementing i at the end */
        if (i < cap) i++;
        return buffer[i];
    }

    /* move to next char in buffer */
    i++;

    /* Load more data if needed. */
    if (i >= cap - copylen) {
        /* Copy tail end of data to the front of the buffer
         * so we don't lose lookahead modifications */
        for (int j=0;j<copylen;j++) {
             buffer[j] = buffer[cap - copylen + j];
        }
        /* Refill buffer */
        size_t read =  fread(buffer+copylen, sizeof(char), BUFLEN - copylen, f);
        if (read == 0) {
            /* No more input so we stop after copylen characters. The last
             * character read will be EOF. */
            dead = 1;
            cap = copylen;
            buffer[cap] = EOF;
        } else {
            /* Move end of buffer marker to new position */
            dead = 0;
            cap = copylen + read;
        }
        /* Current character is now the first in the buffer */
        i = 0;
    }

#if DOOFUS_MODE
    /* What we do here is deparenthesize units of contained length <= 2
     * We thus only deparenthesize (++) and (--), as well as two-digit
     * numbers and things of the form (-1) which conveniently get parsed
     * the same sans parenthesis. (We thus don't need to bother looking
     * inside the parens.)
     *
     * This simplifies the lexing and parsing code at slight cost increase*/
    if (buffer[i] == '(') {
        if (buffer[i+2] == ')') {
            buffer[i] = ' ';
            buffer[i+2] = ' ';
        } else if (buffer[i+3] == ')') {
            buffer[i] = ' ';
            buffer[i+3] = ' ';
        }
    }
#endif

    /* return the next character */
    return buffer[i];
}